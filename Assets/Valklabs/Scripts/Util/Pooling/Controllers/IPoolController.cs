﻿using UnityEngine;

namespace ValkLabs.Util.Pooling
{
    public interface IPoolController<T>
    {
        void InitPool();

        T Get();

        void OnCleanUp();
    }

}
