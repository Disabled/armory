﻿using UnityEngine;

namespace ValkLabs.Util.Pooling
{
    [System.Serializable]
    public class AdvancedMonoPoolController<T> : BaseMonoPoolController<T> where T : MonoBehaviour, IPoolable
    {
        protected override T CreateNewInstance()
        {
            T instance = base.CreateNewInstance();
            if(instance != null)
            {
                instance.OnCreate();
            }
            return instance;
        }

        protected override void OnGetSuccessful(T instance)
        {
            instance.OnSpawn();
            instance.OnDespawnEvent += OnReturn; //Make aware of what pool to return to on despawn when activating
        }

        private void OnReturn(IPoolable poolable)
        {
            if (poolable == null)
            {
                return;
            }

            T instance = poolable as T;
            instance.OnDespawnEvent -= OnReturn; //Remove as we don't need to be listening while in the pool! Listener is re-added once/if pooled again.
            OnReturnItem(instance);
        }

        protected override void CleanPoolInstances()
        {
            if (_pool == null || _allInstances == null)
            {
                return;
            }

            foreach (T item in _allInstances)
            {
                if (item != null)
                {
                    item.OnDespawnEvent -= OnReturn;
                    Object.Destroy(item.gameObject);
                }
            }
        }
    }
}

