﻿using System.Collections.Generic;
using UnityEngine;

namespace ValkLabs.Util.Pooling
{

    public abstract class BaseMonoPoolController<T> : IPoolController<T> where T : MonoBehaviour
    {
        [Header("Pooled Object Prefab")]
        [SerializeField] private T _prefab;

        [Header("Pre-created Instances To Pool")]
        [Tooltip("Can be left empty. Adds objects to pool stack.")]
        [SerializeField] protected List<T> _preCreatedPooledObjects;

        [Header("Pool Settings")]
        [Tooltip("The container the pooled objects will reside in the scene hierarchy when inactive. If intending to have pooled objects live from scene-to-scene, make sure the parent (and this Pool Controller) are on objects that 'dont destroy on load'.")]
        [SerializeField] protected Transform _poolContainer;
        [Tooltip("Set the max amount of pooled objects allowed to be created during run time on Get(). Value of 0 or less will remove any max amount limit.")]
        [SerializeField] protected int _maxPooledObjectsCount = -1;

        protected Stack<T> _pool;
        protected IList<T> _allInstances; //Holds a reference of all the instances made (and pre made) from pooling. This is used for cleanup rather than just checking the stack, as we may want to request to cleanup while a pooled instance is no longer in the stack, ensuring we don't miss that instance.

        #region Init
        /// <summary>
        /// Initialize pool controller.
        /// </summary>
        public virtual void InitPool()
        {
            if (_prefab == null)
            {
                Debug.LogError("Pooling prefab is null!");
                return;
            }

            if (_poolContainer == null)
            {
                Debug.LogError("Pool Container is null!");
                return;
            }

            _allInstances = new List<T>();
            _pool = new Stack<T>();

            //Any pre-created instances will be added to the pooled stack for poolable objects
            if (_preCreatedPooledObjects != null && _preCreatedPooledObjects.Count > 0)
            {
                foreach (T instance in _preCreatedPooledObjects)
                {
                    if (instance != null)
                    {
                        OnPrePopulatedPooledItemInit(instance);
                    }
                }
            }
        }

        protected virtual void OnPrePopulatedPooledItemInit(T instance)
        {
            instance.gameObject.SetActive(false);
            AddToInstanceList(instance);
            Add(instance);
        }
        #endregion

        #region Create
        protected virtual T CreateNewInstance()
        {
            if (_prefab == null)
            {
                return null;
            }

            T instance = Object.Instantiate(_prefab, _poolContainer);
            AddToInstanceList(instance);

            return instance;
        }

        private bool IsAllowedToCreateInstance()
        {
            if (_maxPooledObjectsCount <= 0 || _allInstances.Count < _maxPooledObjectsCount)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region Stack Commands
        protected void Add(T instance)
        {
            if (instance == null || _pool == null)
            {
                Debug.LogError("instance and/or poolstack is null -- can't add to pool");
                return;
            }

            _pool.Push(instance);
        }

        protected T Remove()
        {
            if (_pool == null || _pool.Count == 0)
            {
                return null;
            }

            return _pool.Pop();
        }
        #endregion

        #region Instance List
        protected void AddToInstanceList(T instance)
        {
            instance.transform.SetParent(_poolContainer);
            _allInstances.Add(instance);
        }
        #endregion

        #region Get Pooled Object
        protected abstract void OnGetSuccessful(T instance);

        /// <summary>
        /// Gets an instance of the pooled object from the stack.
        /// </summary>
        public virtual T Get()
        {
            if (_pool == null)
            {
                Debug.LogError("_poolstack is null, cannot get!");
                return null;
            }

            T instance = Remove();
            if (instance == null && IsAllowedToCreateInstance() == true) //No available pooled objects. Create new instance if allowed
            {
                instance = CreateNewInstance();
            }

            if (instance != null)
            {
                OnGetSuccessful(instance);
            }

            return instance;
        }

        /// <summary>
        /// Gets an instance of the pooled object from the stack. Sets location without changing the parent. Consider 'Get(Transform spawnAtTransform, bool isWorldPositionStays)' if you require the pooled object to follow another object.
        /// </summary>
        /// <param name="spawnAt">The location the object will spawn at.</param>
        public T Get(Vector3 spawnAt)
        {
            T instance = Get();
            if (instance != null)
            {
                instance.transform.position = spawnAt;
            }

            return instance;
        }

        /// <summary>
        /// Gets an instance of the pooled object from the stack. Sets location without changing the parent. Consider 'Get(Transform spawnAtTransform, bool isWorldPositionStays, Quaternion rotation)' if you require the pooled object to follow another object.
        /// </summary>
        /// <param name="spawnAt">The location the object will spawn at.</param>
        /// <param name="rotation">The rotation the object will spawn with.</param>
        public T Get(Vector3 spawnAt, Quaternion rotation)
        {
            T instance = Get(spawnAt);
            if (instance != null)
            {
                instance.transform.rotation = rotation;
            }
            return instance;
        }

        /// <summary>
        /// Gets an instance of the pooled object from the stack. Adds object to another object's transform. Consider 'Get(Vector3 spawnAt)' if you don't require the pooled object to follow another transform.
        /// </summary>
        /// <param name="spawnAtTransform">The transform requesting to spawn this object. The spawned object will set this transform as it's parent and zero out it's local position.</param>
        /// <param name="isWorldPositionStays">the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before.</param>
        public T Get(Transform spawnAtTransform, bool isWorldPositionStays)
        {
            T instance = Get();
            if (instance != null)
            {
                instance.transform.SetParent(spawnAtTransform, isWorldPositionStays);
                instance.transform.localPosition = Vector3.zero;
            }

            return instance;
        }

        /// <summary>
        /// Gets an instance of the pooled object from the stack. Adds object to another object's transform. Consider 'Get(Vector3 spawnAt, Quaternion rotation)' if you don't require the pooled object to follow another transform.
        /// </summary>
        /// <param name="spawnAtTransform">The transform requesting to spawn this object. The spawned object will set this transform as it's parent and zero out it's local position.</param>
        /// <param name="isWorldPositionStays">the parent-relative position, scale and rotation are modified such that the object keeps the same world space position, rotation and scale as before.</param>
        /// <param name="rotation">The rotation the object will spawn with. if 'isWorldPositionStays' is true, this will be applied after; resulting in this rotation.</param>
        public T Get(Transform spawnAtTransform, bool isWorldPositionStays, Quaternion rotation)
        {
            T instance = Get();
            if (instance != null)
            {
                instance.transform.SetParent(spawnAtTransform, isWorldPositionStays);
                instance.transform.localPosition = Vector3.zero;
                instance.transform.rotation = rotation;
            }

            return instance;
        }
        #endregion

        #region Callbacks
        protected void OnReturnItem(T instance) {

            if(instance == null)
            {
                return;
            }
            instance.transform.SetParent(_poolContainer);

            Add(instance);
        }
        #endregion

        #region Cleanup
        protected abstract void CleanPoolInstances();

        /// <summary>
        /// Cleans up memory when no longer needing this MonoPoolController and it's pooled instances. Destroys the pooled instances.
        /// </summary>
        public void OnCleanUp()
        {
            CleanPoolInstances();

            _pool?.Clear();
            _pool = null;
            _allInstances?.Clear();
            _allInstances = null;
        }
        #endregion
    }
}

