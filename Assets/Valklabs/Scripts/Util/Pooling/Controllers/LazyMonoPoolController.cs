﻿using UnityEngine;

namespace ValkLabs.Util.Pooling
{
    /// <summary>
    /// A lazy/fast way to setup up pooling. Not as optiminal as the Advanced pooler, but more self managed.
    /// </summary>
    [System.Serializable]
    public class LazyMonoPoolController<T> : BaseMonoPoolController<T> where T : MonoBehaviour
    {
        /// <summary>
        /// Called from the InitPool() function, and only if there are any pre populated objects to be pooled.
        /// </summary>
        /// <param name="instance">The object of a pre pooled from _preCreatedPooledObjects list (If any)</param>
        protected override void OnPrePopulatedPooledItemInit(T instance)
        {
            base.OnPrePopulatedPooledItemInit(instance);
            AddReturnToPoolComponent(instance); //Needed for lazy pool controller to add component to any monobehaviour for it to return to pool once inactive
        }

        protected override void OnGetSuccessful(T instance)
        {
            instance.gameObject.SetActive(true);
        }

        protected override T CreateNewInstance()
        {
            T instance = base.CreateNewInstance();
            if(instance != null)
            {
                AddReturnToPoolComponent(instance);
            }
            return instance;
        }

        private void AddReturnToPoolComponent(T instance)
        {
            if(instance != null && instance.GetComponent<LazyMonoReturnComponent>() == false)
            {
                instance.gameObject.AddComponent<LazyMonoReturnComponent>().Init(delegate { OnReturn(instance); });
            }
        }

        private void OnReturn(T poolable)
        {
            OnReturnItem(poolable);
        }

        protected override void CleanPoolInstances()
        {
            if (_pool == null || _allInstances == null)
            {
                return;
            }

            foreach (T item in _allInstances)
            {
                if (item != null)
                {
                    Object.Destroy(item.gameObject);
                }
            }
        }
    }
}

