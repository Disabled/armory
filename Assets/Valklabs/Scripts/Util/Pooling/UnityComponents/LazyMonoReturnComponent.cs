﻿using System;
using UnityEngine;

namespace ValkLabs.Util.Pooling
{
    [AddComponentMenu("")] //This will hide the class from being visible in the "Add Component" menu in Unity Editor. This class is entirely self managed and automatically added by the lazy controller; no need to add it yourself.
    public class LazyMonoReturnComponent : MonoBehaviour
    {
        private Action<MonoBehaviour> _onReturnToPool;

        public void Init(Action<MonoBehaviour> onReturnToPool)
        {
            _onReturnToPool = onReturnToPool;
        }

        private void OnDisable()
        {
            _onReturnToPool?.Invoke(this); //Returns object to pool
        }

        private void OnDestroy()
        {
            Cleanup();
        }

        private void Cleanup()
        {
            _onReturnToPool = null;
        }
    }
}

