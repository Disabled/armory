﻿using System;

namespace ValkLabs.Util.Pooling
{
    //Give you more control for what happens on Spawn, Despawn, and Create. Only used/required with the Advanced Mono Pool Controller.
    public interface IPoolable
    {
        /// <summary>
        /// Ever only called once when/if an instance is created by the pooler.
        /// </summary>
        void OnCreate();

        /// <summary>
        /// This is where you may want to set your gameobject active, or anything else depending on your pooling needs. 
        /// </summary>
        void OnSpawn();

        /// <summary>
        /// Return object to pool. This is where you may want to set your gameobject inactive, or anything else depending on your pooling needs.
        /// </summary>
        event Action<IPoolable> OnDespawnEvent;
    }

}
